###########################################
#     AUTOMOTIVE SAFETY FOR TTP2050       #
#     COUNTY-LEVEL DISAGGREGATION         #
###########################################

sf_hotspot_non_overlap_crossroad_urban <- st_as_sf(hotspot_non_overlap_crossroad_urban)
sf_hotspot_non_overlap_crossroad_rural <- st_as_sf(hotspot_non_overlap_crossroad_rural)

sf_hotspot_non_overlap_non_crossroad_urban <- st_as_sf(hotspot_non_overlap_non_crossroad_urban)
sf_hotspot_non_overlap_non_crossroad_rural <- st_as_sf(hotspot_non_overlap_non_crossroad_rural)

names(counties_tx) <- tolower(names(counties_tx))

sf_counties_tx <- st_as_sf(counties_tx)

sf_hotspot_non_overlap_crossroad_urban <- st_transform(sf_hotspot_non_overlap_crossroad_urban, crs_for_intersection)
sf_hotspot_non_overlap_crossroad_rural <- st_transform(sf_hotspot_non_overlap_crossroad_rural, crs_for_intersection)

sf_hotspot_non_overlap_non_crossroad_urban <- st_transform(sf_hotspot_non_overlap_non_crossroad_urban, crs_for_intersection)
sf_hotspot_non_overlap_non_crossroad_rural <- st_transform(sf_hotspot_non_overlap_non_crossroad_rural, crs_for_intersection)

sf_counties_tx <- st_transform(sf_counties_tx, crs_for_intersection)

inter_hotspot_crossroad_urban <- st_intersection(sf_hotspot_non_overlap_crossroad_urban, sf_counties_tx)
inter_hotspot_crossroad_rural <- st_intersection(sf_hotspot_non_overlap_crossroad_rural, sf_counties_tx)

inter_hotspot_non_crossroad_urban <- st_intersection(sf_hotspot_non_overlap_non_crossroad_urban, sf_counties_tx)
inter_hotspot_non_crossroad_rural <- st_intersection(sf_hotspot_non_overlap_non_crossroad_rural, sf_counties_tx)

# # Recalculate length and lane-miles
# inter_hotspot_crossroad_urban$length <- st_length(inter_hotspot_crossroad_urban) * 0.0006213712 # Convert from meters to miles
# inter_hotspot_crossroad_rural$length <- st_length(inter_hotspot_crossroad_rural) * 0.0006213712
# 
# inter_hotspot_non_crossroad_urban$length <- st_length(inter_hotspot_non_crossroad_urban) * 0.0006213712
# inter_hotspot_non_crossroad_rural$length <- st_length(inter_hotspot_non_crossroad_rural) * 0.0006213712

save(inter_hotspot_crossroad_urban, inter_hotspot_crossroad_rural,
     inter_hotspot_non_crossroad_urban, inter_hotspot_non_crossroad_rural,
     file = "rda/intersection_counties_roads.rda")

# rm(hotspot_non_overlap_crossroad_rural, hotspot_non_overlap_crossroad_urban,
#    hotspot_non_overlap_non_crossroad_rural, hotspot_non_overlap_non_crossroad_urban,
#    sf_counties_tx, counties_tx,
#    sf_hotspot_non_overlap_crossroad_rural, sf_hotspot_non_overlap_crossroad_urban,
#    sf_hotspot_non_overlap_non_crossroad_rural, sf_hotspot_non_overlap_non_crossroad_urban,
#    states_tx,
#    txdot_roadways_on_system_crossroad, txdot_roadways_on_system_non_crossroad,
#    txdot_districts)

load("rda/intersection_counties_roads.rda")

df_inter_hotspot_crossroad_rural <- as.data.frame(inter_hotspot_crossroad_rural, stringsAsFactors = FALSE)
df_inter_hotspot_crossroad_urban <- as.data.frame(inter_hotspot_crossroad_urban, stringsAsFactors = FALSE)
df_inter_hotspot_non_crossroad_rural <- as.data.frame(inter_hotspot_non_crossroad_rural, stringsAsFactors = FALSE)
df_inter_hotspot_non_crossroad_urban <- as.data.frame(inter_hotspot_non_crossroad_urban, stringsAsFactors = FALSE)

rm(inter_hotspot_crossroad_rural, inter_hotspot_crossroad_urban,
   inter_hotspot_non_crossroad_rural, inter_hotspot_non_crossroad_urban)

df_inter_hotspot_crossroad_rural <- df_inter_hotspot_crossroad_rural[, c(1:(ncol(df_inter_hotspot_crossroad_rural) - 1)),]
df_inter_hotspot_crossroad_urban <- df_inter_hotspot_crossroad_urban[, c(1:(ncol(df_inter_hotspot_crossroad_urban) - 1)),]

df_inter_hotspot_non_crossroad_rural <- df_inter_hotspot_non_crossroad_rural[, c(1:(ncol(df_inter_hotspot_non_crossroad_rural) - 1)),]
df_inter_hotspot_non_crossroad_urban <- df_inter_hotspot_non_crossroad_urban[, c(1:(ncol(df_inter_hotspot_non_crossroad_urban) - 1)),]

# Calculate the urban and rural lane-miles and miles ("len_sec") of unaddressed urban and rural hotspots by functional system.
unaddressed_hotspots_crossroad_urban_counties <- df_inter_hotspot_crossroad_urban %>%
  group_by(countyfp) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))
unaddressed_hotspots_crossroad_urban_counties$countyfp <- paste("48", unaddressed_hotspots_crossroad_urban_counties$countyfp, sep = "")

unaddressed_hotspots_crossroad_rural_counties <- df_inter_hotspot_crossroad_rural %>%
  group_by(countyfp) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))
unaddressed_hotspots_crossroad_rural_counties$countyfp <- paste("48", unaddressed_hotspots_crossroad_rural_counties$countyfp, sep = "")

unaddressed_hotspots_non_crossroad_urban_counties <- df_inter_hotspot_non_crossroad_urban %>%
  group_by(countyfp) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))
unaddressed_hotspots_non_crossroad_urban_counties$countyfp <- paste("48", unaddressed_hotspots_non_crossroad_urban_counties$countyfp, sep = "")

unaddressed_hotspots_non_crossroad_rural_counties <- df_inter_hotspot_non_crossroad_rural %>%
  group_by(countyfp) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))
unaddressed_hotspots_non_crossroad_rural_counties$countyfp <- paste("48", unaddressed_hotspots_non_crossroad_rural_counties$countyfp, sep = "")

# Calculate needs
unaddressed_hotspots_crossroad_urban_counties$needs <- unaddressed_hotspots_crossroad_urban_counties$n * as.numeric(costs_crossroad_urban$cost_per_n)
unaddressed_hotspots_crossroad_rural_counties$needs <- unaddressed_hotspots_crossroad_rural_counties$n * as.numeric(costs_crossroad_rural$cost_per_n)

unaddressed_hotspots_non_crossroad_urban_counties$needs <- unaddressed_hotspots_non_crossroad_urban_counties$len_sec * as.numeric(costs_non_crossroad_urban$cost_per_mile)
unaddressed_hotspots_non_crossroad_rural_counties$needs <- unaddressed_hotspots_non_crossroad_rural_counties$len_sec * as.numeric(costs_non_crossroad_rural$cost_per_mile)

# Write results
write.csv(unaddressed_hotspots_crossroad_urban_counties, file = "output/results_county_crossroad_urban.csv")
write.csv(unaddressed_hotspots_crossroad_rural_counties, file = "output/results_county_crossroad_rural.csv")

write.csv(unaddressed_hotspots_non_crossroad_urban_counties, file = "output/results_county_non_crossroad_urban.csv")
write.csv(unaddressed_hotspots_non_crossroad_rural_counties, file = "output/results_county_non_crossroad_rural.csv")


##################################################
##            NEEDS BY NHS STATUS               ##
##################################################
load("data/txdot_roadways_on_system.rda")
nhs_crosswalk <- as.data.frame(txdot_roadways_on_system[, c("objectid", "sec_nhs")], stringsAsFactors = FALSE)

hotspot_non_overlap_crossroad_urban_nhs <- as.data.frame(merge(hotspot_non_overlap_crossroad_urban, nhs_crosswalk, by = "objectid"), stringsAsFactors = FALSE)
hotspot_non_overlap_crossroad_rural_nhs <- as.data.frame(merge(hotspot_non_overlap_crossroad_rural, nhs_crosswalk, by = "objectid"), stringsAsFactors = FALSE)

hotspot_non_overlap_non_crossroad_urban_nhs <- as.data.frame(merge(hotspot_non_overlap_non_crossroad_urban, nhs_crosswalk, by = "objectid"), stringsAsFactors = FALSE)
hotspot_non_overlap_non_crossroad_rural_nhs <- as.data.frame(merge(hotspot_non_overlap_non_crossroad_rural, nhs_crosswalk, by = "objectid"), stringsAsFactors = FALSE)

hotspot_non_overlap_crossroad_urban_nhs$cat <- NA
hotspot_non_overlap_crossroad_rural_nhs$cat <- NA
hotspot_non_overlap_non_crossroad_urban_nhs$cat <- NA
hotspot_non_overlap_non_crossroad_rural_nhs$cat <- NA

hotspot_non_overlap_crossroad_urban_nhs[which(hotspot_non_overlap_crossroad_urban_nhs$f_system == 1), "cat"] <- "IH"
hotspot_non_overlap_crossroad_urban_nhs[which(hotspot_non_overlap_crossroad_urban_nhs$f_system != 1 & hotspot_non_overlap_crossroad_urban_nhs$sec_nhs != 0), "cat"] <- "non-IH NHS"
hotspot_non_overlap_crossroad_urban_nhs[which(hotspot_non_overlap_crossroad_urban_nhs$sec_nhs == 0), "cat"] <- "non NHS"

hotspot_non_overlap_crossroad_rural_nhs[which(hotspot_non_overlap_crossroad_rural_nhs$f_system == 1), "cat"] <- "IH"
hotspot_non_overlap_crossroad_rural_nhs[which(hotspot_non_overlap_crossroad_rural_nhs$f_system != 1 & hotspot_non_overlap_crossroad_rural_nhs$sec_nhs != 0), "cat"] <- "non-IH NHS"
hotspot_non_overlap_crossroad_rural_nhs[which(hotspot_non_overlap_crossroad_rural_nhs$sec_nhs == 0), "cat"] <- "non NHS"

hotspot_non_overlap_non_crossroad_urban_nhs[which(hotspot_non_overlap_non_crossroad_urban_nhs$f_system == 1), "cat"] <- "IH"
hotspot_non_overlap_non_crossroad_urban_nhs[which(hotspot_non_overlap_non_crossroad_urban_nhs$f_system != 1 & hotspot_non_overlap_non_crossroad_urban_nhs$sec_nhs != 0), "cat"] <- "non-IH NHS"
hotspot_non_overlap_non_crossroad_urban_nhs[which(hotspot_non_overlap_non_crossroad_urban_nhs$sec_nhs == 0), "cat"] <- "non NHS"

hotspot_non_overlap_non_crossroad_rural_nhs[which(hotspot_non_overlap_non_crossroad_rural_nhs$f_system == 1), "cat"] <- "IH"
hotspot_non_overlap_non_crossroad_rural_nhs[which(hotspot_non_overlap_non_crossroad_rural_nhs$f_system != 1 & hotspot_non_overlap_non_crossroad_rural_nhs$sec_nhs != 0), "cat"] <- "non-IH NHS"
hotspot_non_overlap_non_crossroad_rural_nhs[which(hotspot_non_overlap_non_crossroad_rural_nhs$sec_nhs == 0), "cat"] <- "non NHS"


# Calculate the lane-miles and miles ("len_sec") of unaddressed hotspots by functional system.
unaddressed_hotspots_crossroad_urban_nhs <- hotspot_non_overlap_crossroad_urban_nhs %>%
  group_by(cat) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_crossroad_rural_nhs <- hotspot_non_overlap_crossroad_rural_nhs %>%
  group_by(cat) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_urban_nhs <- hotspot_non_overlap_non_crossroad_urban_nhs %>%
  group_by(cat) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_rural_nhs <- hotspot_non_overlap_non_crossroad_rural_nhs %>%
  group_by(cat) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

# Calculate needs
unaddressed_hotspots_crossroad_urban_nhs$needs <- unaddressed_hotspots_crossroad_urban_nhs$n * as.numeric(costs_crossroad_urban$cost_per_n)
unaddressed_hotspots_crossroad_rural_nhs$needs <- unaddressed_hotspots_crossroad_rural_nhs$n * as.numeric(costs_crossroad_rural$cost_per_n)

unaddressed_hotspots_non_crossroad_urban_nhs$needs <- unaddressed_hotspots_non_crossroad_urban_nhs$length * as.numeric(costs_non_crossroad_urban$cost_per_mile)
unaddressed_hotspots_non_crossroad_rural_nhs$needs <- unaddressed_hotspots_non_crossroad_rural_nhs$length * as.numeric(costs_non_crossroad_rural$cost_per_mile)

# Write needs
write.csv(unaddressed_hotspots_crossroad_urban_nhs, file = "output/results_crossroad_urban_nhs_status.csv")
write.csv(unaddressed_hotspots_crossroad_rural_nhs, file = "output/results_crossroad_rural_nhs_status.csv")
write.csv(unaddressed_hotspots_non_crossroad_urban_nhs, file = "output/results_non_crossroad_urban_nhs_status.csv")
write.csv(unaddressed_hotspots_non_crossroad_rural_nhs, file = "output/results_non_crossroad_rural_nhs_status.csv")

###########################
# FM and RM Safety Needs
###########################
load("data/txdot_roadways_on_system.rda")
fm <- as.data.frame(txdot_roadways_on_system[which(txdot_roadways_on_system$hsys == "FM"),] ,stringsAsFactors = FALSE)
rm <- as.data.frame(txdot_roadways_on_system[which(txdot_roadways_on_system$hsys == "RM"),], stringsAsFactors = FALSE)

fm <- as.vector(fm[,c("objectid")])
rm <- as.vector(rm[,c("objectid")])

#########################
# Intersect with hotspots
#########################
hotspot_non_overlap_crossroad_urban_fm <- hotspot_non_overlap_crossroad_urban[which(hotspot_non_overlap_crossroad_urban$objectid %in% fm),]
hotspot_non_overlap_crossroad_rural_fm <- hotspot_non_overlap_crossroad_rural[which(hotspot_non_overlap_crossroad_rural$objectid %in% fm),]
hotspot_non_overlap_non_crossroad_urban_fm <- hotspot_non_overlap_non_crossroad_urban[which(hotspot_non_overlap_non_crossroad_urban$objectid %in% fm),]
hotspot_non_overlap_non_crossroad_rural_fm <- hotspot_non_overlap_non_crossroad_rural[which(hotspot_non_overlap_non_crossroad_rural$objectid %in% fm),]

hotspot_non_overlap_crossroad_urban_rm <- hotspot_non_overlap_crossroad_urban[which(hotspot_non_overlap_crossroad_urban$objectid %in% rm),]
hotspot_non_overlap_crossroad_rural_rm <- hotspot_non_overlap_crossroad_rural[which(hotspot_non_overlap_crossroad_rural$objectid %in% rm),]
hotspot_non_overlap_non_crossroad_urban_rm <- hotspot_non_overlap_non_crossroad_urban[which(hotspot_non_overlap_non_crossroad_urban$objectid %in% rm),]
hotspot_non_overlap_non_crossroad_rural_rm <- hotspot_non_overlap_non_crossroad_rural[which(hotspot_non_overlap_non_crossroad_rural$objectid %in% rm),]

### Calculate the lane-miles and miles ("len_sec") of unaddressed hotspots by functional system.
# Farm-to-Market
unaddressed_hotspots_crossroad_urban_fm <- as.data.frame(hotspot_non_overlap_crossroad_urban_fm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_crossroad_rural_fm <- as.data.frame(hotspot_non_overlap_crossroad_rural_fm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_urban_fm <- as.data.frame(hotspot_non_overlap_non_crossroad_urban_fm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_rural_fm <- as.data.frame(hotspot_non_overlap_non_crossroad_rural_fm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

# Ranch-to-Market
unaddressed_hotspots_crossroad_urban_rm <- as.data.frame(hotspot_non_overlap_crossroad_urban_rm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_crossroad_rural_rm <- as.data.frame(hotspot_non_overlap_crossroad_rural_rm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_urban_rm <- as.data.frame(hotspot_non_overlap_non_crossroad_urban_rm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_rural_rm <- as.data.frame(hotspot_non_overlap_non_crossroad_rural_rm, stringsAsFactors = FALSE) %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

### Calculate needs
# Farm-to-Market
unaddressed_hotspots_crossroad_urban_fm$needs <- unaddressed_hotspots_crossroad_urban_fm$n * as.numeric(costs_crossroad_urban$cost_per_n)
unaddressed_hotspots_crossroad_rural_fm$needs <- unaddressed_hotspots_crossroad_rural_fm$n * as.numeric(costs_crossroad_rural$cost_per_n)
unaddressed_hotspots_non_crossroad_urban_fm$needs <- unaddressed_hotspots_non_crossroad_urban_fm$length * as.numeric(costs_non_crossroad_urban$cost_per_mile)
unaddressed_hotspots_non_crossroad_rural_fm$needs <- unaddressed_hotspots_non_crossroad_rural_fm$length * as.numeric(costs_non_crossroad_rural$cost_per_mile)

# Ranch-to-Market
unaddressed_hotspots_crossroad_urban_rm$needs <- unaddressed_hotspots_crossroad_urban_rm$n * as.numeric(costs_crossroad_urban$cost_per_n)
unaddressed_hotspots_crossroad_rural_rm$needs <- unaddressed_hotspots_crossroad_rural_rm$n * as.numeric(costs_crossroad_rural$cost_per_n)
unaddressed_hotspots_non_crossroad_urban_rm$needs <- unaddressed_hotspots_non_crossroad_urban_rm$length * as.numeric(costs_non_crossroad_urban$cost_per_mile)
unaddressed_hotspots_non_crossroad_rural_rm$needs <- unaddressed_hotspots_non_crossroad_rural_rm$length * as.numeric(costs_non_crossroad_rural$cost_per_mile)

### Results ###
results <- as.data.frame(matrix(NA, 2, ncol = 4))
names(results) <- c("crossroad_urban", "crossroad_rural", "non_crossroad_urban", "non_crossroad_rural")
rownames(results) <- c("fm", "rm")

results[1, 1] = unaddressed_hotspots_crossroad_urban_fm$needs
results[1, 2] = unaddressed_hotspots_crossroad_rural_fm$needs
results[1, 3] = unaddressed_hotspots_non_crossroad_urban_fm$needs
results[1, 4] = unaddressed_hotspots_non_crossroad_rural_fm$needs

results[2, 1] = unaddressed_hotspots_crossroad_urban_rm$needs
results[2, 2] = unaddressed_hotspots_crossroad_rural_rm$needs
results[2, 3] = unaddressed_hotspots_non_crossroad_urban_rm$needs
results[2, 4] = unaddressed_hotspots_non_crossroad_rural_rm$needs

write.csv(results, "output/results_fm_rm_needs.csv")
