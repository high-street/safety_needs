###################################
#                                 #
#             FUNCTIONS           #
#                                 #
###################################

identify_hotspots <- function(hotspot_threshold_crash_crossroad,
                              hotspot_threshold_fatal_crossroad,
                              hotspot_threshold_injury_crossroad,
                              
                              hotspot_threshold_crash_non_crossroad,
                              hotspot_threshold_fatal_non_crossroad,
                              hotspot_threshold_injury_non_crossroad,
                              
                              network_crossroad = txdot_roadways_on_system_crossroad,
                              network_non_crossroad = txdot_roadways_on_system_non_crossroad,
                              
                              project_buffer = utp_buffer,
                              
                              forecast_crash_crossroad,
                              forecast_crash_non_crossroad,
                              
                              forecast_fatal_crossroad,
                              forecast_fatal_non_crossroad,
                              
                              forecast_injury_crossroad,
                              forecast_injury_non_crossroad,
                              
                              forecast_fatal_total_crossroad,
                              forecast_fatal_total_non_crossroad,
                              
                              forecast_injury_total_crossroad,
                              forecast_injury_total_non_crossroad,
                              
                              yr
                              
                              # horizon
                              ){
 
  past_hotspot_crossroad = data.frame()
  past_hotspot_non_crossroad = data.frame()
  
  # for(i in 0:horizon){
  # Make summary sheet from which to calculate needs
  column = yr - 2020 + 2 #+ horizon
  temp_sheet_crossroad <- as.data.frame(summary_inter_crossroad_2$objectid)
  temp_sheet_non_crossroad <- as.data.frame(summary_inter_non_crossroad_2$objectid)
  temp_forecast_crash_crossroad = forecast_crash_crossroad[, c(1, column)]
  temp_forecast_fatal_crossroad = forecast_fatal_crossroad[, c(1, column)]
  temp_forecast_injury_crossroad = forecast_injury_crossroad[, c(1, column)]
  temp_forecast_fatal_total_crossroad = forecast_fatal_total_crossroad[, c(1, column)]
  temp_forecast_injury_total_crossroad = forecast_injury_total_crossroad[, c(1, column)]
  temp_forecast_crash_non_crossroad = forecast_crash_non_crossroad[, c(1, column)]
  temp_forecast_fatal_non_crossroad = forecast_fatal_non_crossroad[, c(1, column)]
  temp_forecast_injury_non_crossroad = forecast_injury_crossroad[, c(1, column)]
  temp_forecast_fatal_total_non_crossroad = forecast_fatal_total_non_crossroad[, c(1, column)]
  temp_forecast_injury_total_non_crossroad = forecast_injury_total_crossroad[, c(1, column)]
  
  names(temp_sheet_crossroad)[1] <- "objectid"
  names(temp_sheet_non_crossroad)[1] <- "objectid"
  names(temp_forecast_crash_crossroad) <- c("objectid", paste("crash_crossroad_", yr, sep = ""))
  names(temp_forecast_fatal_crossroad) <- c("objectid", paste("fatal_crossroad_", yr, sep = ""))
  names(temp_forecast_injury_crossroad) <- c("objectid", paste("injury_crossroad_", yr, sep = ""))
  names(temp_forecast_fatal_total_crossroad) <- c("objectid", paste("fatal_total_crossroad_", yr, sep = ""))
  names(temp_forecast_injury_total_crossroad) <- c("objectid", paste("injury_total_crossroad_", yr, sep = ""))
  names(temp_forecast_crash_non_crossroad) <- c("objectid", paste("crash_non_crossroad_", yr, sep = ""))
  names(temp_forecast_fatal_non_crossroad) <- c("objectid", paste("fatal_non_crossroad_", yr, sep = ""))
  names(temp_forecast_injury_non_crossroad) <- c("objectid", paste("injury_non_crossroad_", yr, sep = ""))
    
  temp_sheet_crossroad <- full_join(temp_sheet_crossroad, temp_forecast_crash_crossroad, by = "objectid")
  temp_sheet_crossroad <- full_join(temp_sheet_crossroad, temp_forecast_fatal_crossroad, by = "objectid")
  temp_sheet_crossroad <- full_join(temp_sheet_crossroad, temp_forecast_injury_crossroad, by = "objectid")
  temp_sheet_crossroad <- full_join(temp_sheet_crossroad, temp_forecast_fatal_total_crossroad, by = "objectid")
  temp_sheet_crossroad <- full_join(temp_sheet_crossroad, temp_forecast_injury_total_crossroad, by = "objectid")
  temp_sheet_non_crossroad <- full_join(temp_sheet_non_crossroad, temp_forecast_crash_non_crossroad, by = "objectid")
  temp_sheet_non_crossroad <- full_join(temp_sheet_non_crossroad, temp_forecast_fatal_non_crossroad, by = "objectid")
  temp_sheet_non_crossroad <- full_join(temp_sheet_non_crossroad, temp_forecast_injury_non_crossroad, by = "objectid")
  temp_sheet_non_crossroad <- full_join(temp_sheet_non_crossroad, temp_forecast_fatal_total_non_crossroad, by = "objectid")
  temp_sheet_non_crossroad <- full_join(temp_sheet_non_crossroad, temp_forecast_injury_total_non_crossroad, by = "objectid")
  # temp_sheet_non_crossroad <- cbind(forecast_crash_non_crossroad[, 1], forecast_crash_non_crossroad[, column], forecast_fatal_non_crossroad[, column], forecast_injury_non_crossroad[, column], forecast_fatal_non_crossroad[, 1], forecast_injury_non_crossroad[, 1])
    
  # Hotspots qualify based on crash rates, death rates, or serious injury rates.  If the later two, then there must have been at least 3 serious injuries or deaths over a five year period.
  # Crossroad
  hotspot_objectid_crossroad <- temp_sheet_crossroad[which(temp_sheet_crossroad[, 2] > hotspot_threshold_crash_crossroad |
                                                             ((temp_sheet_crossroad[, 3] > hotspot_threshold_fatal_crossroad |
                                                                 temp_sheet_crossroad[, 4] > hotspot_threshold_injury_crossroad) &
                                                                (temp_sheet_crossroad[, 5] * 5 + temp_sheet_crossroad[, 6] * 5) >= 3)), "objectid"]
    
    
  # Non-crossroad
  hotspot_objectid_non_crossroad <- temp_sheet_non_crossroad[which(temp_sheet_non_crossroad[, 2] > hotspot_threshold_crash_non_crossroad |
                                                                     ((temp_sheet_non_crossroad[, 3] > hotspot_threshold_fatal_non_crossroad |
                                                                         temp_sheet_non_crossroad[, 4] > hotspot_threshold_injury_non_crossroad) &
                                                                        (temp_sheet_non_crossroad[, 5] * 5 + temp_sheet_non_crossroad[, 6] * 5) >= 3)), "objectid"]
    
  # hotspot_objectid_crossroad <- hotspot_objectid_crossroad[which(hotspot_objectid_crossroad %!in% past_hotspot_crossroad),]
  # hotspot_objectid_non_crossroad <- hotspot_objectid_non_crossroad[which(hotspot_objectid_non_crossroad %!in% past_hotspot_non_crossroad),]
  # 
  # past_hotspot_crossroad = past_hotspot_crossroad[rbind(past_hotspot_crossroad, hotspot_objectid_crossroad)]
  # past_hotspot_non_crossroad = past_hotspot_non_crossroad[rbind(past_hotspot_non_crossroad, hotspot_objectid_non_crossroad)]
    
  # Identify hotspots
  hotspot_crossroad <- network_crossroad[which(network_crossroad$objectid %in% hotspot_objectid_crossroad),]
  hotspot_non_crossroad <- network_non_crossroad[which(network_non_crossroad$objectid %in% hotspot_objectid_non_crossroad),]
    
  #####################################
  #
  #     PROJECTS
  #
  #####################################
    
  # Identify projects that improve safety at intersections / crossroads.
  rows_crossroad <- grep("(INTERSECTION)|(SIGNALS)|(SIGNAL)|(TN LN)|(TS)|(TURN)|(BEACON)", project_buffer@data$type_of_wo) # (signify turn lanes, signals, and traffic signals)
  utp_crossroad <- project_buffer@data[rows_crossroad,]
  # Identify projects that improve safety on non-intersection / crossroad road segments.
  utp_non_crossroad <- project_buffer@data[-rows_crossroad,]
  rows_exclude <- grep("REST AREA", utp_non_crossroad$type_of_wo) # Exclude rest areas because their safety benefits are very diffuse; not related to a specific segment or even road.
  utp_non_crossroad <- utp_non_crossroad[-rows_exclude,]
  rm(rows_crossroad, rows_exclude)
    
  sf_hotspot_crossroad <- st_as_sf(hotspot_crossroad)
  sf_hotspot_non_crossroad <- st_as_sf(hotspot_non_crossroad)
    
  inter_hotspot_crossroad_proj <- st_intersection(sf_hotspot_crossroad, sf_utp_buffer)
  inter_hotspot_non_crossroad_proj <- st_intersection(sf_hotspot_non_crossroad, sf_utp_buffer)
    
  cols_to_keep <- c("objectid", "avg_annual_crash_rate", "avg_annual_death_rate", "avg_annual_serious_injury_rate", "isn", "csj_number", 
                    "district_n", "county_num", "highway_nu", "proj_lengt", "proj_class", "constructi", "type_of_wo", "perc_crossroad", "uan")
    
  inter_hotspot_crossroad_proj <- inter_hotspot_crossroad_proj[, cols_to_keep]
  inter_hotspot_non_crossroad_proj <- inter_hotspot_non_crossroad_proj[, cols_to_keep]
    
  # Clean up
  rm(cols_to_keep)
    
  # Hotspots overlapping with projects
  objectid_overlap_crossroad <- unique(inter_hotspot_crossroad_proj$objectid)
  objectid_overlap_non_crossroad <- unique(inter_hotspot_non_crossroad_proj$objectid)
    
  length(objectid_overlap_crossroad)
  length(objectid_overlap_non_crossroad)
    
  # Hotspots not overlapping with projects
  objectid_no_overlap_crossroad <- unique(hotspot_crossroad$objectid)
  objectid_no_overlap_non_crossroad <- unique(hotspot_non_crossroad$objectid)
    
  objectid_no_overlap_crossroad <- objectid_no_overlap_crossroad[which(objectid_no_overlap_crossroad %!in% objectid_overlap_crossroad == TRUE)]
  objectid_no_overlap_non_crossroad <- objectid_no_overlap_non_crossroad[which(objectid_no_overlap_non_crossroad %!in% objectid_overlap_non_crossroad == TRUE)]
    
  length(objectid_no_overlap_crossroad)
  length(objectid_no_overlap_non_crossroad)
    
  hotspot_non_overlap_crossroad <- hotspot_crossroad[which(hotspot_crossroad$objectid %in% objectid_no_overlap_crossroad),]
  hotspot_non_overlap_crossroad <- hotspot_crossroad[which(hotspot_crossroad$objectid %in% objectid_no_overlap_crossroad),]
    
  hotspot_non_overlap_non_crossroad <- hotspot_non_crossroad[which(hotspot_non_crossroad$objectid %in% objectid_no_overlap_non_crossroad),]
  hotspot_non_overlap_non_crossroad <- hotspot_non_crossroad[which(hotspot_non_crossroad$objectid %in% objectid_no_overlap_non_crossroad),]
    
  # Categorize non-overlapping hotspots as urban / rural
  hotspot_non_overlap_crossroad_urban <- hotspot_non_overlap_crossroad[which(hotspot_non_overlap_crossroad$uan != 0),]
  hotspot_non_overlap_crossroad_rural <- hotspot_non_overlap_crossroad[which(hotspot_non_overlap_crossroad$uan == 0),]
    
  hotspot_non_overlap_non_crossroad_urban <- hotspot_non_overlap_non_crossroad[which(hotspot_non_overlap_non_crossroad$uan != 0),]
  hotspot_non_overlap_non_crossroad_rural <- hotspot_non_overlap_non_crossroad[which(hotspot_non_overlap_non_crossroad$uan == 0),]
    
  # Calculate the urban and rural lane-miles and miles ("len_sec") of unaddressed urban and rural hotspots by functional system.
  unaddressed_hotspots_crossroad_urban <- hotspot_non_overlap_crossroad_urban@data %>%
    summarize(n = n(),
              ln_miles = sum(ln_miles, na.rm = TRUE),
              length = sum(length, na.rm = TRUE),
              len_sec = sum(len_sec, na.rm = TRUE))
    
  unaddressed_hotspots_crossroad_rural <- hotspot_non_overlap_crossroad_rural@data %>%
    summarize(n = n(),
              ln_miles = sum(ln_miles, na.rm = TRUE),
              length = sum(length, na.rm = TRUE),
              len_sec = sum(len_sec, na.rm = TRUE))
    
  unaddressed_hotspots_non_crossroad_urban <- hotspot_non_overlap_non_crossroad_urban@data %>%
    summarize(n = n(),
              ln_miles = sum(ln_miles, na.rm = TRUE),
              length = sum(length, na.rm = TRUE),
              len_sec = sum(len_sec, na.rm = TRUE))
    
  unaddressed_hotspots_non_crossroad_rural <- hotspot_non_overlap_non_crossroad_rural@data %>%
    summarize(n = n(),
              ln_miles = sum(ln_miles, na.rm = TRUE),
              length = sum(length, na.rm = TRUE),
              len_sec = sum(len_sec, na.rm = TRUE))
  
  # TCD = traffic control device, source: https://www.txdot.gov/inside-txdot/division/research-technology/pooled-fund-program.html 
  # RH = railroad-highway crossings, source: http://onlinemanuals.txdot.gov/txdotmanuals/tfc/state_highway_safety_performance_plan.htm
  # SRA = safety rest area, source: http://my35.org/news/newsletters/2018/05/architects.htm 
    
  # Calculate the needs as a function of average cost per mile of urban or rural road
  needs_crossroad_urban <- as.numeric(costs_crossroad_urban$cost_per_n) * as.numeric(unaddressed_hotspots_crossroad_urban$n)
  needs_crossroad_rural <- as.numeric(costs_crossroad_rural$cost_per_n) * as.numeric(unaddressed_hotspots_crossroad_rural$len_sec)
    
  needs_non_crossroad_urban <- as.numeric(costs_non_crossroad_urban$cost_per_mile) * as.numeric(unaddressed_hotspots_non_crossroad_urban$len_sec)
  needs_non_crossroad_rural <- as.numeric(costs_non_crossroad_rural$cost_per_mile) * as.numeric(unaddressed_hotspots_non_crossroad_rural$len_sec)
    
  needs_total <- needs_crossroad_rural + needs_crossroad_urban + needs_non_crossroad_rural + needs_non_crossroad_urban
    
  ########################
  # Make a table to summarize the safety needs
  ########################
  results <- as.data.frame(matrix(NA, nrow = 1, ncol = 6))
  names(results) <- c("year", "intersection_urban", "intersection_rural", "non_intersection_urban", "non_intersection_rural", "total")
    
  results[1, 1] = yr
  results[1, 2] = needs_crossroad_urban
  results[1, 3] = needs_crossroad_rural
  results[1, 4] = needs_non_crossroad_urban
  results[1, 5] = needs_non_crossroad_rural
  results[1, 6] = needs_total
    
  return(results)
  # }
  

}
