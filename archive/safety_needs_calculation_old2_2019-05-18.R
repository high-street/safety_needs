###############################
#                             #
#  SAFETY NEEDS - TTP 2050    #
#                             #
###############################


#####################
# Identify crash hotspots
#####################

### Primary method to identify hotspots.  It takes the top 5% of road segments by crash rate.
hotspot_threshold_crash_crossroad <- as.numeric(quantile(summary_inter_crossroad$avg_annual_crash_rate, threshold))
hotspot_threshold_fatal_crossroad <- as.numeric(quantile(summary_inter_crossroad$avg_annual_death_rate, threshold))
hotspot_threshold_injury_crossroad <- as.numeric(quantile(summary_inter_crossroad$avg_annual_serious_injury_rate, threshold))
# Hotspots qualify based on crash rates, death rates, or serious injury rates.  If the later two, then there must have been at least 3 serious injuries or deaths over a five year period.
hotspot_objectid_crossroad <- summary_inter_crossroad[which(summary_inter_crossroad$avg_annual_crash_rate > hotspot_threshold_crash_crossroad |
                                                              ((summary_inter_crossroad$avg_annual_death_rate > hotspot_threshold_fatal_crossroad |
                                                              summary_inter_crossroad$avg_annual_serious_injury_rate > hotspot_threshold_injury_crossroad) &
                                                              (summary_inter_crossroad$avg_annual_deaths * 5 + summary_inter_crossroad$avg_annual_serious_injuries * 5) >= 3)), "objectid"]

hotspot_threshold_crash_non_crossroad <- as.numeric(quantile(summary_inter_non_crossroad$avg_annual_crash_rate, threshold))
hotspot_threshold_fatal_non_crossroad <- as.numeric(quantile(summary_inter_non_crossroad$avg_annual_death_rate, threshold))
hotspot_threshold_injury_non_crossroad <- as.numeric(quantile(summary_inter_non_crossroad$avg_annual_serious_injury_rate, threshold))
# Hotspots qualify based on crash rates, death rates, or serious injury rates.  If the later two, then there must have been at least 3 serious injuries or deaths over a five year period.
hotspot_objectid_non_crossroad <- summary_inter_non_crossroad[which(summary_inter_non_crossroad$avg_annual_crash_rate > hotspot_threshold_crash_non_crossroad |
                                                                ((summary_inter_non_crossroad$avg_annual_death_rate > hotspot_threshold_fatal_non_crossroad |
                                                                summary_inter_non_crossroad$avg_annual_serious_injury_rate > hotspot_threshold_injury_non_crossroad) &
                                                                (summary_inter_non_crossroad$avg_annual_deaths * 5 + summary_inter_non_crossroad$avg_annual_serious_injuries * 5) >= 3)), "objectid"]
# Identify hotspots
hotspot_crossroad <- txdot_roadways_on_system_crossroad[which(txdot_roadways_on_system_crossroad$objectid %in% hotspot_objectid_crossroad),]
hotspot_non_crossroad <- txdot_roadways_on_system_non_crossroad[which(txdot_roadways_on_system_non_crossroad$objectid %in% hotspot_objectid_non_crossroad),]

# # Make hotspot shapefile
# writeOGR(obj = hotspot, dsn = "Shapefile Outputs", layer = paste("auto_hotspots_threshold", threshold, sep = "_"), driver = "ESRI Shapefile")

### Alternate method for identifying hotspots.  This one takes road segments that 
### are above avg for crash rate, fatality rate, or serious injury rate.
### This alternative method identifies about 8% of road segments.
# # Crashes
# mean_crash_rate <- mean(summary_inter$avg_annual_crash_rate)
# sd(summary_inter$avg_annual_crash_rate)
# max(summary_inter$avg_annual_crash_rate)
# 
# nrow(summary_inter[which(summary_inter$avg_annual_crash_rate > mean_crash_rate),]) / nrow(summary_inter)
# 
# # Fatalities
# mean_death_rate <- mean(summary_inter$avg_annual_death_rate)
# sd(summary_inter$avg_annual_death_rate)
# max(summary_inter$avg_annual_death_rate)
# 
# nrow(summary_inter[which(summary_inter$avg_annual_death_rate > mean_death_rate),]) / nrow(summary_inter)
# 
# # Serious injuries
# mean_serious_injury_rate <- mean(summary_inter$avg_annual_serious_injury_rate)
# sd(summary_inter$avg_annual_serious_injury_rate)
# max(summary_inter$avg_annual_serious_injury_rate)
# 
# nrow(summary_inter[which(summary_inter$avg_annual_serious_injury_rate > mean_serious_injury_rate),]) / nrow(summary_inter)
# 
# # A hotspot has a rate above the mean on crashes, fatalities, or serious injuries
# hotspot_objectid <- summary_inter[which(summary_inter$avg_annual_crash_rate > mean_crash_rate |
#                                           summary_inter$avg_annual_death_rate > mean_death_rate |
#                                           summary_inter$avg_annual_serious_injury_rate > mean_serious_injury_rate), "objectid"]
# 
# rm(mean_crash_rate, mean_death_rate, mean_serious_injury_rate)
# 
# temp3 <- txdot_roadways_on_system_merge[which(txdot_roadways_on_system_merge@data$objectid %in% hotspot_objectid),]
# nrow(temp3) / nrow(txdot_roadways_on_system_merge)
# 
# rm(temp3)

### Create a shapefile of hotspots
# txdot_roadways_on_system_auto_hotspots <- txdot_roadways_on_system[which(txdot_roadways_on_system$objectid %in% hotspot_objectid),]

#####################################
# 
#     PROJECTS
#
#####################################

# Identify projects that improve safety at intersections / crossroads.
rows_crossroad <- grep("(INTERSECTION)|(SIGNALS)|(SIGNAL)|(TN LN)|(TS)|(TURN)|(BEACON)", utp_buffer@data$type_of_wo) # (signify turn lanes, signals, and traffic signals)
utp_crossroad <- utp_buffer@data[rows_crossroad,]
# Identify projects that improve safety on non-intersection / crossroad road segments.
utp_non_crossroad <- utp_buffer@data[-rows_crossroad,]
rows_exclude <- grep("REST AREA", utp_non_crossroad$type_of_wo) # Exclude rest areas because their safety benefits are very diffuse; not related to a specific segment or even road.
utp_non_crossroad <- utp_non_crossroad[-rows_exclude,]
rm(rows_crossroad, rows_exclude)

utp_non_crossroad_urban <- utp_non_crossroad[which(!is.na(utp_non_crossroad$mpo_code)),]
utp_non_crossroad_rural <- utp_non_crossroad[which(is.na(utp_non_crossroad$mpo_code)),]

utp_crossroad_urban <- utp_crossroad[which(!is.na(utp_crossroad$mpo_code)),]
utp_crossroad_rural <- utp_crossroad[which(is.na(utp_crossroad$mpo_code)),]

### Calculate UTP-related costs for intersection / crossroad and non-intersections / crossroad in urban and rural areas
costs_crossroad_urban <- utp_crossroad_urban %>%
  summarize(n = n(),
            construction_cost = sum(constructi) * 1.04, # Convert 2019 USD to 2020 USD at 4% inflation rate
            cost_per_n = construction_cost / n
  )
costs_crossroad_rural <- utp_crossroad_rural %>%
  summarize(n = n(),
            construction_cost = sum(constructi) * 1.04,
            cost_per_n = construction_cost / n
  )
costs_non_crossroad_urban <- utp_non_crossroad_urban %>%
  summarize(n = n(),
            prj_ln_miles = sum(ln_miles),
            construction_cost = sum(constructi) * 1.04,
            proj_lengt = sum(proj_lengt),
            cost_per_n = construction_cost / n,
            cost_per_ln_mile = construction_cost / prj_ln_miles,
            cost_per_mile = construction_cost / proj_lengt
  )
costs_non_crossroad_rural <- utp_non_crossroad_rural %>%
  summarize(n = n(),
            prj_ln_miles = sum(ln_miles),
            construction_cost = sum(constructi) * 1.04,
            proj_lengt = sum(proj_lengt),
            cost_per_n = construction_cost / n,
            cost_per_ln_mile = construction_cost / prj_ln_miles,
            cost_per_mile = construction_cost / proj_lengt
  )

save(costs_crossroad_rural, costs_crossroad_urban,
     costs_non_crossroad_rural, costs_non_crossroad_urban,
     file = "rda/safety_project_costs.rda")

# Identify the hotspots that do and that don't intersect with a safety project
# hotspot_crossroad_urban <- hotspot_crossroad[which(hotspot_crossroad$uan != 0),]
# hotspot_crossroad_rural <- hotspot_crossroad[which(hotspot_crossroad$uan == 0),]
# 
# hotspot_non_crossroad_urban <- hotspot_non_crossroad[which(hotspot_non_crossroad$uan != 0),]
# hotspot_non_crossroad_rural <- hotspot_non_crossroad[which(hotspot_non_crossroad$uan == 0),]

sf_hotspot_crossroad <- st_as_sf(hotspot_crossroad)
sf_hotspot_non_crossroad <- st_as_sf(hotspot_non_crossroad)

inter_hotspot_crossroad_proj <- st_intersection(sf_hotspot_crossroad, sf_utp_buffer)
inter_hotspot_non_crossroad_proj <- st_intersection(sf_hotspot_non_crossroad, sf_utp_buffer)

cols_to_keep <- c("objectid", "avg_annual_crash_rate", "avg_annual_death_rate", "avg_annual_serious_injury_rate", "isn", "csj_number", 
                  "district_n", "county_num", "highway_nu", "proj_lengt", "proj_class", "constructi", "type_of_wo", "perc_crossroad", "uan")

inter_hotspot_crossroad_proj <- inter_hotspot_crossroad_proj[, cols_to_keep]
inter_hotspot_non_crossroad_proj <- inter_hotspot_non_crossroad_proj[, cols_to_keep]

# Clean up
rm(cols_to_keep)

# Hotspots overlapping with projects
objectid_overlap_crossroad <- unique(inter_hotspot_crossroad_proj$objectid)
objectid_overlap_non_crossroad <- unique(inter_hotspot_non_crossroad_proj$objectid)

length(objectid_overlap_crossroad)
length(objectid_overlap_non_crossroad)

# Hotspots not overlapping with projects
objectid_no_overlap_crossroad <- unique(hotspot_crossroad$objectid)
objectid_no_overlap_non_crossroad <- unique(hotspot_non_crossroad$objectid)

objectid_no_overlap_crossroad <- objectid_no_overlap_crossroad[which(objectid_no_overlap_crossroad %!in% objectid_overlap_crossroad == TRUE)]
objectid_no_overlap_non_crossroad <- objectid_no_overlap_non_crossroad[which(objectid_no_overlap_non_crossroad %!in% objectid_overlap_non_crossroad == TRUE)]

length(objectid_no_overlap_crossroad)
length(objectid_no_overlap_non_crossroad)

hotspot_non_overlap_crossroad <- hotspot_crossroad[which(hotspot_crossroad$objectid %in% objectid_no_overlap_crossroad),]
hotspot_non_overlap_crossroad <- hotspot_crossroad[which(hotspot_crossroad$objectid %in% objectid_no_overlap_crossroad),]

hotspot_non_overlap_non_crossroad <- hotspot_non_crossroad[which(hotspot_non_crossroad$objectid %in% objectid_no_overlap_non_crossroad),]
hotspot_non_overlap_non_crossroad <- hotspot_non_crossroad[which(hotspot_non_crossroad$objectid %in% objectid_no_overlap_non_crossroad),]

# Categorize non-overlapping hotspots as urban / rural
hotspot_non_overlap_crossroad_urban <- hotspot_non_overlap_crossroad[which(hotspot_non_overlap_crossroad$uan != 0),]
hotspot_non_overlap_crossroad_rural <- hotspot_non_overlap_crossroad[which(hotspot_non_overlap_crossroad$uan == 0),]

hotspot_non_overlap_non_crossroad_urban <- hotspot_non_overlap_non_crossroad[which(hotspot_non_overlap_non_crossroad$uan != 0),]
hotspot_non_overlap_non_crossroad_rural <- hotspot_non_overlap_non_crossroad[which(hotspot_non_overlap_non_crossroad$uan == 0),]

# Calculate the urban and rural lane-miles and miles ("len_sec") of unaddressed urban and rural hotspots by functional system.
unaddressed_hotspots_crossroad_urban <- hotspot_non_overlap_crossroad_urban@data %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_crossroad_rural <- hotspot_non_overlap_crossroad_rural@data %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_urban <- hotspot_non_overlap_non_crossroad_urban@data %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

unaddressed_hotspots_non_crossroad_rural <- hotspot_non_overlap_non_crossroad_rural@data %>%
  summarize(n = n(),
            ln_miles = sum(ln_miles, na.rm = TRUE),
            length = sum(length, na.rm = TRUE),
            len_sec = sum(len_sec, na.rm = TRUE))

# TCD = traffic control device, source: https://www.txdot.gov/inside-txdot/division/research-technology/pooled-fund-program.html 
# RH = railroad-highway crossings, source: http://onlinemanuals.txdot.gov/txdotmanuals/tfc/state_highway_safety_performance_plan.htm
# SRA = safety rest area, source: http://my35.org/news/newsletters/2018/05/architects.htm 

# Calculate the needs as a function of average cost per mile of urban or rural road
needs_crossroad_urban <- as.numeric(costs_crossroad_urban$cost_per_n) * as.numeric(unaddressed_hotspots_crossroad_urban$n)
needs_crossroad_rural <- as.numeric(costs_crossroad_rural$cost_per_n) * as.numeric(unaddressed_hotspots_crossroad_rural$len_sec)

needs_non_crossroad_urban <- as.numeric(costs_non_crossroad_urban$cost_per_mile) * as.numeric(unaddressed_hotspots_non_crossroad_urban$len_sec)
needs_non_crossroad_rural <- as.numeric(costs_non_crossroad_rural$cost_per_mile) * as.numeric(unaddressed_hotspots_non_crossroad_rural$len_sec)

needs_total <- needs_crossroad_rural + needs_crossroad_urban + needs_non_crossroad_rural + needs_non_crossroad_urban

########################
# Make a table to summarize the safety needs
########################
results <- as.data.frame(matrix(NA, nrow = (2050 - 2019), ncol = 6))
names(results) <- c("year", "intersection_urban", "intersection_rural", "non_intersection_urban", "non_intersection_rural", "total_million_usd")

for(i in 1:nrow(results)){
  results[i, 1] = 2019 + i
  results[i, 2] = needs_crossroad_urban / nrow(results)
  results[i, 3] = needs_crossroad_rural / nrow(results)
  results[i, 4] = needs_non_crossroad_urban / nrow(results)
  results[i, 5] = needs_non_crossroad_rural / nrow(results)
  results[i, 6] = needs_total / nrow(results)
  }

# write.csv(results, file = "output/automotive_needs_results_05.csv")

######################################
# Inflate needs for future years traffic
######################################
# Read in VMT growth rates by county
vmt_growth <- read_xlsx(path = "data/county_percent_diff_24hr_VMT_20_to_50.xlsx", sheet = "data")
vmt_growth$annual_rate <- vmt_growth$percent_diff / (2050 - 2020)
vmt_growth$fips <- as.double(vmt_growth$fips)
names(vmt_growth)[4] <- "full_fips"

# Merge safety information with a table linking roads and counties so that we can later link
# county-level VMT growth rates wit hthe roads
summary_inter_crossroad_2 <- merge(summary_inter_crossroad, df_inter_road_counties, by = "objectid")
summary_inter_non_crossroad_2 <- merge(summary_inter_non_crossroad, df_inter_road_counties, by = "objectid")

# Merge the above file with county-level VMT growth so that we can grow VMT on each road segment and intersection / crossroad.
summary_inter_crossroad_2 <- merge(summary_inter_crossroad_2, vmt_growth, by = "full_fips")
summary_inter_non_crossroad_2 <- merge(summary_inter_non_crossroad_2, vmt_growth, by = "full_fips")

# Create IDs for each crash type so that later we can select just the road segments and intersections that have had crashes, fatalities, or serious injuries
crash_id_crossroad <- summary_inter_crossroad_2[which(summary_inter_crossroad_2$avg_annual_crash_rate > 0), "objectid"]
crash_id_non_crossroad <- summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$avg_annual_crash_rate > 0), "objectid"]

fatal_id_crossroad <- summary_inter_crossroad_2[which(summary_inter_crossroad_2$avg_annual_death_rate > 0), "objectid"]
fatal_id_non_crossroad <- summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$avg_annual_death_rate > 0), "objectid"]

injury_id_crossroad <- summary_inter_crossroad_2[which(summary_inter_crossroad_2$avg_annual_serious_injury_rate > 0), "objectid"]
injury_id_non_crossroad <- summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$avg_annual_serious_injury_rate > 0), "objectid"]

# Forecast VMT
# first, make an empty matrix for road segments and crossroads / intersections that we'll populate with the VMT
# The length is the number of road segments and crossroads / intersections.
vmt_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_crossroad_2), length(2020:2050)))
vmt_non_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_non_crossroad_2), length(2020:2050)))

# Name the columns in these empty matrices.
names(vmt_crossroad) <- rep(x = "avg_annual_vmt", 2050 - 2020 + 1)
names(vmt_non_crossroad) <- rep(x = "avg_annual_vmt", 2050 - 2020 + 1)

# Add the VMT for the first year, and then apply the annual growth rate for subsequent years.
vmt_crossroad[, 1] = summary_inter_crossroad_2$avg_annual_vmt
vmt_non_crossroad[, 1] = summary_inter_non_crossroad_2$avg_annual_vmt
for(yr in 2:length(2020:2050)){
  vmt_crossroad[, yr] = vmt_crossroad[, yr - 1] * (1 + summary_inter_crossroad_2[, "annual_rate"])
  vmt_non_crossroad[, yr] = vmt_non_crossroad[, yr - 1] * (1 + summary_inter_non_crossroad_2[, "annual_rate"])
}
# vmt_crossroad[, (2050 - 2020 + 2)] = vmt_crossroad[, (2050 - 2020 + 1)] * (1 + summary_inter_crossroad_2[, "annual_rate"])
# vmt_non_crossroad[, (2050 - 2020 + 2)] = vmt_non_crossroad[, (2050 - 2020 + 1)] * (1 + summary_inter_non_crossroad_2[, "annual_rate"])

# Add object id column to the VMT matrix so that we can link it with other road and intersection information.
vmt_crossroad <- cbind(summary_inter_crossroad_2$objectid, vmt_crossroad)
vmt_non_crossroad <- cbind(summary_inter_non_crossroad_2$objectid, vmt_non_crossroad)

# Rename the column we just added as the "objectid"
names(vmt_crossroad)[1] <- "objectid"
names(vmt_non_crossroad)[1] <- "objectid"

## Forecast crash rates
## First, make an empty data frame for the following:
## - Predicted crash rates at crossroads and on road segments
## - Predicted fatality rates at crossroads and on road segments
## - Predicted injury rates at crossroads and on road segments.
## - Predicted annual number of fatality at crossroads and on road segments.
## - Predicted annual number of serious injuries at crossroads and on road segments.
# Make predicted crash rate data frame.
forecast_crash_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% 
                              crash_id_crossroad),]), length(2020:2050)))
forecast_crash_non_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in%
                              crash_id_non_crossroad),]), length(2020:2050)))

# Add column names for each year.
names(forecast_crash_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_crash_non_crossroad) <- paste("year_", 2020:2050, sep = "")

# Add a column for objectid
forecast_crash_crossroad <- cbind(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% crash_id_crossroad),"objectid"], forecast_crash_crossroad)
forecast_crash_non_crossroad <- cbind(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% crash_id_non_crossroad), "objectid"], forecast_crash_non_crossroad)

# Predict the crash rates
temp <- vmt_crossroad[which(vmt_crossroad$objectid %in% crash_id_crossroad),]
for(yr in 2:length(2020:2051)){
  column <- as.data.frame(temp[, yr], stringsAsFactors = FALSE)
  names(column) <- "avg_annual_vmt"
  # names(column)[1] <- "objectid"
  forecast_crash_crossroad[, yr] = predict(object = mod1, column)
}
names(forecast_crash_crossroad)[1] <- "objectid"

temp <- vmt_non_crossroad[which(vmt_non_crossroad$objectid %in% crash_id_non_crossroad),]
for(yr in 2:length(2020:2051)){
  column <- as.data.frame(temp[, yr], stringsAsFactors = FALSE)
  names(column) <- "avg_annual_vmt"
  forecast_crash_non_crossroad[, yr] = predict(object = mod4, column)
}
names(forecast_crash_non_crossroad)[1] <- "objectid"

## Fatality rates
# Make empty data frame for predicted fatality rates.
forecast_fatal_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% 
                                              fatal_id_crossroad),]), length(2020:2050)))
forecast_fatal_non_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% 
                                              fatal_id_non_crossroad),]), length(2020:2050)))
# Make empty data frame for predicted annual fatalities.
forecast_fatal_total_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% 
                                              fatal_id_crossroad),]), length(2020:2050)))
forecast_fatal_total_non_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% 
                                              fatal_id_non_crossroad),]), length(2020:2050)))

# Name columns
names(forecast_fatal_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_fatal_non_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_fatal_total_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_fatal_total_non_crossroad) <- paste("year_", 2020:2050, sep = "")

# Add a column for objectid
forecast_fatal_crossroad <- cbind(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% 
                                  fatal_id_crossroad), "objectid"], forecast_fatal_crossroad)
forecast_fatal_non_crossroad <- cbind(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% 
                                  fatal_id_non_crossroad),"objectid"], forecast_fatal_non_crossroad)
forecast_fatal_total_crossroad <- cbind(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% 
                                  fatal_id_crossroad), "objectid"], forecast_fatal_total_crossroad)
forecast_fatal_total_non_crossroad <- cbind(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% 
                                  fatal_id_non_crossroad),"objectid"], forecast_fatal_total_non_crossroad)

# Name the objectid column
names(forecast_fatal_crossroad)[1] <- "objectid"
names(forecast_fatal_non_crossroad)[1] <- "objectid"
names(forecast_fatal_total_crossroad)[1] <- "objectid"
names(forecast_fatal_total_non_crossroad)[1] <- "objectid"

# Predict fatality rates
temp <- vmt_crossroad[which(vmt_crossroad$objectid %in% fatal_id_crossroad),]
for(yr in 2:length(2020:2051)){
  column <- as.data.frame(temp[, yr], stringsAsFactors = FALSE)
  names(column) <- "avg_annual_vmt"
  forecast_fatal_crossroad[, yr] = predict(object = mod2, column)
}

temp <- vmt_crossroad[which(vmt_crossroad$objectid %in% fatal_id_non_crossroad),]
for(yr in 2:length(2020:2051)){
  column <- as.data.frame(temp[, yr], stringsAsFactors = FALSE)
  names(column) <- "avg_annual_vmt"
  forecast_fatal_non_crossroad[, yr] = predict(object = mod5, column)
}

## Forecast serious injury rates
# Make empty data frame for predicted serious injury rates.
forecast_injury_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% injury_id_crossroad),]), length(2020:2050)))
forecast_injury_non_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% injury_id_non_crossroad),]), length(2020:2050)))
# Make empty data frame for predicted annual serious injuries.
forecast_injury_total_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% injury_id_crossroad),]), length(2020:2050)))
forecast_injury_total_non_crossroad <- as.data.frame(matrix(NA, nrow(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% injury_id_non_crossroad),]), length(2020:2050)))

# Name columns
names(forecast_injury_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_injury_non_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_injury_total_crossroad) <- paste("year_", 2020:2050, sep = "")
names(forecast_injury_total_non_crossroad) <- paste("year_", 2020:2050, sep = "")

# Add column for objectid
forecast_injury_crossroad <- cbind(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% injury_id_crossroad), "objectid"], forecast_injury_crossroad)
forecast_injury_non_crossroad <- cbind(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% injury_id_non_crossroad), "objectid"], forecast_injury_non_crossroad)
forecast_injury_total_crossroad <- cbind(summary_inter_crossroad_2[which(summary_inter_crossroad_2$objectid %in% injury_id_crossroad), "objectid"], forecast_injury_total_crossroad)
forecast_injury_total_non_crossroad <- cbind(summary_inter_non_crossroad_2[which(summary_inter_non_crossroad_2$objectid %in% injury_id_non_crossroad), "objectid"], forecast_injury_total_non_crossroad)

# Predict serious injury rates
temp <- vmt_crossroad[which(vmt_crossroad$objectid %in% injury_id_crossroad),]
for(yr in 2:length(2020:2051)){
  column <- as.data.frame(temp[, yr], stringsAsFactors = FALSE)
  names(column) <- "avg_annual_vmt"
  forecast_injury_crossroad[, yr] = predict(object = mod3, column)
}
names(forecast_injury_crossroad)[1] <- "objectid"

temp <- vmt_non_crossroad[which(vmt_non_crossroad$objectid %in% injury_id_non_crossroad),]
for(yr in 2:length(2020:2051)){
  column <- as.data.frame(temp[, yr], stringsAsFactors = FALSE)
  names(column) <- "avg_annual_vmt"
  forecast_injury_non_crossroad[, yr] = predict(object = mod6, column)
}
names(forecast_injury_non_crossroad)[1] <- "objectid"

names(forecast_injury_total_crossroad)[1] <- "objectid"
names(forecast_injury_total_non_crossroad)[1] <- "objectid"

# # Clean up
# rm(summary_inter_crossroad_2, summary_inter_non_crossroad_2)

#####################################
# New Autonomous and Connected
# Vehicle Safety Effects
#####################################
# In this section we'll scale down the predicted crash, fatality, and serious injury rates to account for forecasted AV / CV adoption
# Read in adoption rate
cav_effects <- read_xlsx("data/cav adoption rates.xlsx", "Sheet1")
cav_effects <- cav_effects[, 1:3] # Just keep the columns that we need
names(cav_effects) <- tolower(names(cav_effects)) # Lowercase all names for consistency

# Reduce crash rates for CAV adoption
# for(yr in 2:length(2020:2051)){
#   apply(forecast_crash_crossroad, 1, function(x){
#     forecast_crash_crossroad[, yr] * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
#   })
# }
# for(yr in 2:length(2020:2051)){
#   sapply(forecast_crash_crossroad[, yr], function(x){
#     forecast_crash_crossroad[, yr] * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
#   })
#   # forecast_crash_crossroad[, yr] = forecast_crash_crossroad[, yr] * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
# }

# Reduce predicted crash rates at intersections
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_crash_crossroad)){
    forecast_crash_crossroad[j, yr] = exp(forecast_crash_crossroad[j, yr]) * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
  }
}

# for(yr in 2:length(2020:2051)){
#   sapply(X = forecast_crash_crossroad$yr, 1, FUN = function(x){
#     forecast_crash_crossroad$yr = forecast_crash_crossroad$yr * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
#   })
# }

# Reduce predicted crash rates on road segments
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_crash_non_crossroad)){
    forecast_crash_non_crossroad[j, yr] = exp(forecast_crash_non_crossroad[j, yr]) * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
  }
}

# Reduce fatality rates at intersections
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_fatal_crossroad)){
    forecast_fatal_crossroad[j, yr] = exp(forecast_fatal_crossroad[j, yr]) * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
  }
}

# Reduce fatality rates on road segments
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_fatal_non_crossroad)){
    forecast_fatal_non_crossroad[j, yr] = export(forecast_fatal_non_crossroad[j, yr]) * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
  }
}

# Calculate the total number of fatalities at intersections based on reduced fatality rates
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_fatal_total_crossroad)){
    forecast_fatal_total_crossroad[j, yr] = forecast_fatal_crossroad[j, yr] * vmt_crossroad[j, yr] / (100 * 10 ^ 6)
  }
}

# Calculate the total number of fatalities on road segments based on reduced fatality rates
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_fatal_total_non_crossroad)){
    forecast_fatal_total_non_crossroad[j, yr] = forecast_fatal_non_crossroad[j, yr] * vmt_crossroad[j, yr] / (100 * 10 ^ 6)
  }
}

# Reduce serious injury rates at intersections
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_injury_crossroad)){
    forecast_injury_crossroad[j, yr] = exp(forecast_injury_crossroad[j, yr]) * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
  }
}

# Reduce serious injury rates on road segments
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_injury_non_crossroad)){
    forecast_injury_non_crossroad[j, yr] = exp(forecast_injury_non_crossroad[j, yr]) * (1 - cav_effects[yr - 1, "adoption"] * 0.9 * 0.94)
  }
}

# Calculate the annual number of serious injuries at crossroads
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_injury_total_crossroad)){
    forecast_injury_total_crossroad[j, yr] = forecast_injury_crossroad[j, yr] * vmt_crossroad[j, yr] / (100 * 10 ^ 6)
  }
}

# Calculate the annual number of serious injuries on road segments
for(yr in 2:length(2020:2051)){
  for(j in 1:nrow(forecast_injury_total_non_crossroad)){
    forecast_injury_total_non_crossroad[j, yr] = forecast_injury_non_crossroad[j, yr] * vmt_crossroad[j, yr] / (100 * 10 ^ 6)
  }
}

# # Reduce serious injury rates for CAV adoption
# for(yr in 2:length(2020:2051)){
#   for(j in 1:nrow(forecast_injury_crossroad)){
#     forecast_injury_crossroad[j, yr] = forecast_injury_crossroad[j, yr] * (1 - cav_effects[yr - 1, "adoption"]) * 0.9 * 0.94
#   }
# }
# 
# 
# Save these forecasts
save(forecast_crash_crossroad, forecast_crash_non_crossroad,
     forecast_fatal_crossroad, forecast_fatal_non_crossroad,
     forecast_injury_crossroad, forecast_injury_non_crossroad,

     forecast_fatal_total_crossroad, forecast_fatal_non_crossroad,
     forecast_injury_total_crossroad, forecast_injury_total_non_crossroad,
     
     hotspot_threshold_crash_crossroad, hotspot_threshold_fatal_crossroad, hotspot_threshold_injury_crossroad,
     hotspot_threshold_crash_non_crossroad, hotspot_threshold_fatal_non_crossroad, hotspot_threshold_injury_non_crossroad,
     
     summary_inter_crossroad_2, summary_inter_non_crossroad_2,
     
     costs_crossroad_urban, costs_crossroad_rural,
     costs_non_crossroad_urban, costs_non_crossroad_rural,
     
     file = "rda/safety_needs_interim_files_4.rda")

# ### Primary method to identify hotspots.  It takes the top 5% of road segments by crash rate.
# hotspot_threshold_av <- hotspot_threshold
# hotspot_objectid_av <- summary_inter_av[which(summary_inter_av$avg_annual_crash_rate > hotspot_threshold_av), "objectid"]
# 
# # Identify hotspots
# hotspot_av <- txdot_roadways_on_system_merge[which(txdot_roadways_on_system_merge$objectid %in% hotspot_objectid_av),]
# 
# sf_hotspot_av <- st_as_sf(hotspot_av)
# 
# inter_hotspot_proj_av <- st_intersection(sf_hotspot_av, sf_utp_buffer)
# 
# cols_to_keep <- c("objectid", "avg_annual_crash_rate", "avg_annual_death_rate", "avg_annual_serious_injury_rate", "isn", "csj_number", 
#                   "district_n", "county_num", "highway_nu", "proj_lengt", "proj_class", "constructi", "type_of_wo", "perc_crossroad", "uan")
# 
# inter_hotspot_proj_av <- inter_hotspot_proj_av[, cols_to_keep]
# 
# # Clean up
# rm(cols_to_keep)
# 
# # Hotspots overlapping with projects
# objectid_overlap_av <- unique(inter_hotspot_proj_av$objectid)
# length(objectid_overlap_av)
# # Hotspots not overlapping with projects
# objectid_no_overlap_av <- unique(hotspot_av$objectid)
# objectid_no_overlap_av <- objectid_no_overlap_av[which(objectid_no_overlap_av %!in% objectid_overlap_av == TRUE)]
# length(objectid_no_overlap_av)
# 
# # Categorize non-overlapping hotspots by urban / rural and crossroad / non-crossroad
# hotspot_non_overlap_av <- hotspot_av[which(hotspot_av$objectid %in% objectid_no_overlap_av),]
# 
# hotspot_non_overlap_crossroad_urban_av <- hotspot_non_overlap_av[which(hotspot_non_overlap_av$uan != 0 & hotspot_non_overlap_av$perc_crossroad >= 0.5),]
# hotspot_non_overlap_crossroad_rural_av <- hotspot_non_overlap_av[which(hotspot_non_overlap_av$uan == 0 & hotspot_non_overlap_av$perc_crossroad >= 0.5),]
# 
# hotspot_non_overlap_non_crossroad_urban_av <- hotspot_non_overlap_av[which(hotspot_non_overlap_av$uan != 0 & hotspot_non_overlap_av$perc_crossroad < 0.5),]
# hotspot_non_overlap_non_crossroad_rural_av <- hotspot_non_overlap_av[which(hotspot_non_overlap_av$uan == 0 & hotspot_non_overlap_av$perc_crossroad < 0.5),]
# 
# # Calculate the urban and rural lane-miles and miles ("len_sec") of unaddressed urban and rural hotspots by functional system.
# unaddressed_hotspots_crossroad_urban_av <- hotspot_non_overlap_crossroad_urban_av@data %>%
#   summarize(n = n(),
#             ln_miles = sum(ln_miles, na.rm = TRUE),
#             length = sum(length, na.rm = TRUE),
#             len_sec = sum(len_sec, na.rm = TRUE))
# 
# unaddressed_hotspots_crossroad_rural_av <- hotspot_non_overlap_crossroad_rural_av@data %>%
#   summarize(n = n(),
#             ln_miles = sum(ln_miles, na.rm = TRUE),
#             length = sum(length, na.rm = TRUE),
#             len_sec = sum(len_sec, na.rm = TRUE))
# 
# unaddressed_hotspots_non_crossroad_urban_av <- hotspot_non_overlap_non_crossroad_urban_av@data %>%
#   summarize(n = n(),
#             ln_miles = sum(ln_miles, na.rm = TRUE),
#             length = sum(length, na.rm = TRUE),
#             len_sec = sum(len_sec, na.rm = TRUE))
# 
# unaddressed_hotspots_non_crossroad_rural_av <- hotspot_non_overlap_non_crossroad_rural_av@data %>%
#   summarize(n = n(),
#             ln_miles = sum(ln_miles, na.rm = TRUE),
#             length = sum(length, na.rm = TRUE),
#             len_sec = sum(len_sec, na.rm = TRUE))
# 
# # Calculate the needs as a function of average cost per mile of urban or rural road
# needs_crossroad_urban_av <- as.numeric(costs_crossroad_urban$cost_per_n) * as.numeric(unaddressed_hotspots_crossroad_urban_av$n)
# needs_crossroad_rural_av <- as.numeric(costs_crossroad_rural$cost_per_n) * as.numeric(unaddressed_hotspots_crossroad_rural_av$len_sec)
# 
# needs_non_crossroad_urban_av <- as.numeric(costs_non_crossroad_urban$cost_per_mile) * as.numeric(unaddressed_hotspots_non_crossroad_urban_av$len_sec)
# needs_non_crossroad_rural_av <- as.numeric(costs_non_crossroad_rural$cost_per_mile) * as.numeric(unaddressed_hotspots_non_crossroad_rural_av$len_sec)
# 
# needs_total_av <- needs_crossroad_rural_av + needs_crossroad_urban_av + needs_non_crossroad_rural_av + needs_non_crossroad_urban_av
# 
# #############################
# # Make a table of AV results
# #############################
# results_all <- as.data.frame(matrix(NA, nrow = (5), ncol = 3))
# names(results_all) <- c("needs type", "baseline", "av future")
# 
# results_all[1, 1] = "urban intersections"
# results_all[1, 2] = needs_crossroad_urban
# results_all[1, 3] = needs_crossroad_urban_av
#   
# results_all[2, 1] = "rural intersections"
# results_all[2, 2] = needs_crossroad_rural
# results_all[2, 3] = needs_crossroad_rural_av
# 
# results_all[3, 1] = "urban non-intersections"
# results_all[3, 2] = needs_non_crossroad_urban
# results_all[3, 3] = needs_non_crossroad_urban_av
# 
# results_all[4, 1] = "rural non-intersections"
# results_all[4, 2] = needs_non_crossroad_rural
# results_all[4, 3] = needs_non_crossroad_rural_av
# 
# results_all[5, 1] = "total"
# results_all[5, 2] = needs_total
# results_all[5, 3] = needs_total_av
# 
# write.csv(results_all, paste("output/results_all_", threshold, ".csv", sep = ""))

# Clean up
rm(utp_crossroad, utp_crossroad_rural, utp_crossroad_urban,
   utp_non_crossroad, utp_non_crossroad_rural, utp_non_crossroad_urban)
rm(df_inter_districts_roads, inter_districts_roads)

########
# NOW WE NEED TO PREDICT FUTURE YEAR CRASHES, HOTSPOTS AND NEEDS BASED ON VMT
########
# First derive a logistic equation relating crash rates and VMT
# Then assume some decline in crash rates due to tech improvements
# Apply the equation to VMT from the SAM model
# ######################
# # Make Leaflet Map
# ######################
# ### Map of Fatality Rates Overlaid with Safety Projects ###
# # Load data so we can make map without previous steps
# load("rda/safety_needs_interim_files_3.rda")
# 
# txdot_roadways_on_system_merge_mapping <- spTransform(txdot_roadways_on_system_merge, crs_for_mapping)
# 
# # Abbreviate ("abb) txdot roads for only those with a positive fatality rate.
# txdot_roadways_abb <- txdot_roadways_on_system_merge[which(txdot_roadways_on_system_merge@data$avg_annual_death_rate > 0),]
# txdot_roadways_abb <- spTransform(txdot_roadways_abb, crs_for_mapping)
# 
# utp_buffer_mapping <- spTransform(utp_buffer, crs_for_mapping)
# 
# save(utp_buffer_mapping, txdot_roadways_abb, txdot_roadways_on_system_merge_mapping, file = "rda/data_for_mapping.rda")
# 
# 
# 
# load("rda/data_for_mapping.rda")
# load("rda/data_for_mapping_states_counties_districts.rda")
# 
# colors_deaths <- colorNumeric(palette = "YlOrRd", domain = c(min(txdot_roadways_abb@data$avg_annual_death_rate, na.rm = TRUE), 100), na.color = "black")
# x <- 1:100
# roadway_map_2 <- leaflet() %>%
#   addTiles(providers$CartoDB.Positron) %>%
#   # addPolylines(data = txdot_roadways_on_system_merge_mapping[which(txdot_roadways_on_system_merge_mapping@data$avg_annual_death_rate == 0),],
#   #              smoothFactor = 5, fillOpacity = 1, color = "grey", weight = 2) %>%
#   addPolylines(data = txdot_roadways_abb, smoothFactor = 5, fillOpacity = 1, color = ~colors_deaths(txdot_roadways_abb@data$avg_annual_death_rate),
#                weight = 5, highlight = highlightOptions(weight = 10, color = "blue"), group = "fatality rates"
#                # ,
#                # label = paste0(round(txdot_roadways_abb@data$avg_annual_death_rate, 2))
#   ) %>%
#   addPolylines(data = txdot_roadways_abb[which(txdot_roadways_abb@data$avg_annual_death_rate > 100),], smoothFactor = 5, fillOpacity = 1, color = "red", weight = 5,
#                highlight = highlightOptions(weight = 10, color = "blue")
#                # ,
#                # label = paste0(round(txdot_roadways_abb@data$avg_annual_death_rate, 2))
#   ) %>%
#   addPolygons(data = utp_buffer_mapping, smoothFactor = 1, fillOpacity = 1, color = "#336666", weight = 2) %>% # Colors, http://www.sthda.com/english/wiki/colors-in-r
#   addLegend("topright", pal = colors_deaths, values = x, opacity = 1)
# 
# roadway_map_2
# 
